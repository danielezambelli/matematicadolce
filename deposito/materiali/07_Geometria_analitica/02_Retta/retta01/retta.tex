% (c) 2014 Daniele Zambelli - daniele.zambelli@gmail.com

\chapter{Rette nel piano cartesiano}

\section{Equazioni lineari in due variabili}
\label{sec:retta_equazionilineari}

Abbiamo visto che tutte le equazioni del tipo:~\(ax+b=0\) hanno una soluzione
se \(a\neq0\). Ma sulle equazioni lineari (di primo grado) con due incognite, 
cosa possiamo dire? Consideriamo l'equazione:~\(3x + 2y - 6 = 0\) 
ha una soluzione? Ma prima ancora, cosa significa \emph{una} soluzione per 
questa equazione? La soluzione per una equazione in due incognite  non è un 
numero, ma una coppia di numeri il primo da mettere al posto della~\(x\) e il
secondo da mettere al posto della~\(y\) per rendere vera l'uguaglianza.
Possiamo quindi precisare la seguente definizione:

\begin{definizione}
 La soluzione di un'equazione a due incognite è la coppia ordinata di numeri 
 che sostituiti ordinatamente alle incognite rendono vera l'uguaglianza.
\end{definizione}

Si possono trovare molte soluzione di questa equazione, due sono 
semplici da trovare:~\((0; 3)\) e~\((2; 0)\). Si possono verificare facilmente:

\[3 \cdot 0 + 2 \cdot 3 - 6 = 0\]
\[3 \cdot 2 + 2 \cdot 0 - 6 = 0\]

Ne esistono altre?

\begin{inaccessibleblock}[Figura: TODO]
 \begin{figure}[h]
 \centering
 \begin{minipage}[]{.50\textwidth}
\begin{tabular}{ll}
\dots & \dots \\
\dots & \dots \\
\dots & \dots \\
\dots & \dots \\
\((0; ~3)\)   & \(3 \cdot 0 + 2 \cdot 3 - 6 = 0\)     \\
\((2; ~0)\)   & \(3 \cdot 2 + 2 \cdot 0 - 6 = 0\)     \\
\((4;~-3)\)   & \(3 \cdot 4 + 2 \cdot (-3) - 6 = 0\)  \\
\((6;~-6)\)   & \(3 \cdot 6 + 2 \cdot (-6) - 6 = 0\)  \\
\((8;~-9)\)   & \(3 \cdot 8 + 2 \cdot (-9) - 6 = 0\)  \\
\((10;~-12)\) & \(3 \cdot 10 + 2 \cdot (-12) - 6 = 0\)
\end{tabular}
  \caption{Soluzioni equazione.}\label{fig:soluzioniequazione}
 \end{minipage}
 \begin{minipage}[]{.40\textwidth}
  \centering\input{\folder lbr/fig001_puntiallineati.pgf}
  \caption{I corrispondenti punti nel piano.}\label{fig:puntiallineati}
 \end{minipage}
\end{figure}
\end{inaccessibleblock}


Sapresti individuare la regola con la quale ho costruito le soluzioni? 
Sapresti aggiungere altre soluzioni che precedono quelle trovate da me?

In generale una equazione lineare in due incognite ha infinite soluzioni che 
sono coppie di numeri. Ma abbiamo già visto che una coppia di numeri 
rappresenta un punto nel piano cartesiano quindi ogni soluzione rappresenta 
un punto del piano vedi figura~\ref{fig:puntiallineati}.

Possiamo osservare che i punti sono tutti allineati, ma cosa succede 
\emph{tra} due punti? Per renderci più agevole il calcolo modifichiamo
l'equazione di partenza ottenendo una equazione equivalente del 
tipo:~\(y = \dots\):

\[3x + 2y - 6 = 0 \Leftrightarrow 2y = -3x +6 \Leftrightarrow 
 y = -\frac{3}{2}x +3\]

Possiamo costruire una tabella inserendo nella prima colonna dei valori~\(x\) 
scelti da noi e nella seconda i corrispondenti valori di~\(y\) calcolati, 
magari con l'uso della calcolatrice. Poi riportiamo questi valori in un 
piano cartesiano.

\begin{inaccessibleblock}[Figura: TODO]
 \begin{figure}[h]
 \centering
 \begin{minipage}[]{.50\textwidth}
  \begin{center}
   \begin{tabular}{l|l}
    x   & y \\
    \hline
    0   & 3    \\
    0,5 & 2,25 \\
    1   & 1,5  \\
    1,5 & 0,75 \\
    2   & 0
   \end{tabular}
  \end{center}
  \caption{Altre soluzioni.}\label{fig:altresoluzioni}
 \end{minipage}
 \begin{minipage}[]{.40\textwidth}
  \centering\input{\folder lbr/fig002_ingrandimento.pgf}
  \caption{Altri punti.}\label{fig:ingrandimento}
 \end{minipage}
\end{figure}
\end{inaccessibleblock}

Tra due punti calcolati possiamo inserirne quanti vogliamo, ma saranno sempre 
allineati con gli altri.

Si può dimostrare che tutte le soluzioni dell'equazione sono punti allineati 
e che tutti i punti che sono allineati con due qualunque di quella retta hanno
coordinate che sono soluzioni di quell'equazione. 

\begin{inaccessibleblock}[Figura: TODO]
 \begin{figure}[h]
 \centering\input{\folder lbr/fig003_retta01.pgf}
 \caption{Retta di equazione:~\(3x + 2y - 6 = 0\).}\label{fig:retta01}
\end{figure}
\end{inaccessibleblock}

I matematici dicono che c'è una \emph{corrispondenza biunivoca} tra le 
soluzioni di quell'equazione e i punti di quella retta per cui dicono che 
quella equazione è l'\emph{equazione della retta} e che quella retta è il 
\emph{grafico dell'equazione}.

\section{Equazioni della retta}
\label{sec:retta_equazioni}

Nel paragrafo precedente abbiamo scritto l'equazione della retta in due modi 
diversi. A questi due modi di scrivere l'equazione sono stati dati dei nomi:

\begin{itemize}
 \item \(3x + 2y - 6 = 0\):~ equazione implicita;
 \item \(y = -\frac{3}{2}x +3\):~ equazione esplicita.
\end{itemize}

In generale un'equazione implicita è un'equazione nella forma:

\[ax + by + c = 0\]

e un'equazione esplicita è un'equazione nella forma:

\[y = mx + q\]

dove~\(a,~b,~c,~m,~q\) sono dei \emph{parametri} numerici mentre~\(x,~y\) sono 
delle variabili.

\begin{itemize}
 \item \(x\)~è la variabile a cui diamo noi dei valori e si chiama 
  variabile \emph{indipendente};
 \item \(y\)~è la variabile il cui valore viene calcolato e si chiama 
  variabile \emph{dipendente}.
\end{itemize}

Cosa succede se nell'equazione implicita~\(a\)~o~\(b\) valgono zero?
Otteniamo delle equazioni senza la~\(x\) o senza la~\(y\). 
Possiamo osservare che anche le equazioni di primo grado con una sola 
variabile rappresentano delle rette:

\begin{itemize}
 \item la retta \(s\) di equazione \(y=-2\) è l'insieme dei punti del piano 
  che hanno l'ordinata uguale a~\(-2\) e qualunque ascissa;
 \item la retta \(t\) di equazione \(y=3\) è l'insieme dei punti del piano 
  che hanno l'ordinata uguale a~\(3\) e qualunque ascissa;
 \item la retta \(q\) di equazione \(x=-4\) è l'insieme dei punti del piano 
  che hanno l'ascissa uguale a~\(-4\) e qualunque ordinata;
 \item la retta \(r\) di equazione \(x=1\) è l'insieme dei punti del piano 
  che hanno l'ascissa uguale a~\(1\) e qualunque ordinata.
\end{itemize}

\begin{inaccessibleblock}[Figura: TODO]
%  \begin{figure}[h]
 \begin{minipage}[]{.50\textwidth}
  \begin{center}\input{\folder lbr/fig004_rettey.pgf}\end{center}
%   \caption{Rette parallele all'asse x.}\label{fig:rettey}
 \end{minipage}
 \begin{minipage}[]{.40\textwidth}
  \begin{center}\input{\folder lbr/fig005_rettex.pgf}\end{center}
%   \caption{Rette parallele all'asse y.}\label{fig:rettex}
 \end{minipage}
% \end{figure}
\end{inaccessibleblock}

In conclusione l'equazione~\(ax + by + c = 0\) al variare dei 
parametri~\(a\),~\(b\),~\(c\), rappresenta tutte le rette del piano.

\section{Come disegnare le rette}
\label{sec:retta_disegno}

Quando vogliamo disegnare una retta partendo dalla sua equazione, possiamo
applicare la seguente procedura:

\begin{procedura}
 Per disegnare una retta:
 \begin{enumeratea}
  \item ricava l'equazione esplicita~\(y=mx+q\)
  \item riempi una tabella con alcuni valori di~\(x\) scelti da te e i 
   corrispondenti valori di~\(y\) calcolati;
  \item per ogni coppia~\((x;~y)\), disegna un punto sul piano cartesiano;
  \item disegna una retta che passi per quei punti.
 \end{enumeratea}
\end{procedura}

Consideriamo un altro esempio. 

\begin{procedura}
 disegna la retta che ha per equazione:~\(x+2y+6=0\):
 \begin{enumeratea}
  \item l'equazione esplicita è~\(y=- \frac{1}{2}x - 3\)
  \item Nel calcolo, ogni valore di~\(x\) dovrà essere diviso per due, 
   quindi, per~\(x\) scegli valori pari che sono più comodi, costruisci la 
   tabella e calcola i corrispondenti valori di~\(y\), 
   vedi figura~\ref{fig:tabella};
  \item disegna nel piano cartesiano i punti che ci stanno;
  \item disegna la retta che passa per quei punti, 
   vedi figura~\ref{fig:retta02}.
 \end{enumeratea}
\end{procedura}

\begin{inaccessibleblock}[Figura: TODO]
 \begin{figure}[h]
 \centering
 \begin{minipage}[]{.50\textwidth}
  \begin{center}
   \begin{tabular}{l|l}
    x   & y \\
    \hline
    -6 & -6 \\
    -4 & -5 \\
    -2 & -4 \\
     0 & -3 \\
     2 & -2 \\
     4 & -1 \\
     6 & -0
   \end{tabular}
  \end{center}
  \caption{Tabella.}\label{fig:tabella}
 \end{minipage}
 \begin{minipage}[]{.40\textwidth}
   \centering\input{\folder lbr/fig006_retta02.pgf}
  \caption{Disegno di una retta.}\label{fig:retta02}
 \end{minipage}
\end{figure}
\end{inaccessibleblock}

Ma ho proprio bisogno di tutti quei punti? Per individuare una retta 
bastano~2 punti quindi noi ne useremo...~3! In questo modo se 
i punti non appariranno allineati sapremo che abbiamo commesso un errore o 
nel calcolo o nel disegno. Un punto ci serve come controllo 
(come l'ultimo carattere del codice fiscale).

\section{Coefficienti dell'equazione esplicita}
\label{sec:retta_coefficienti}

Prima di procedere dobbiamo procurarci un po' di esempi su cui ragionare.
Disegna, in un piano cartesiano, le seguenti rette:

\begin{multicols}{3}
 \TabPositions{0.6cm}
 \begin{enumeratea}
 \item \(y=-\frac{1}{2}x + 2\)
 \item \(y=-\frac{2}{3}x + 2\)
 \item \(y=-{3}x + 2\)
 \item \(y={2}x + 2\)
 \item \(y=\frac{4}{3}x + 2\)
 \item \(y=\frac{1}{3}x + 2\)
 \end{enumeratea}
\end{multicols}

Disegna in un altro piano cartesiano queste altre rette:

\begin{multicols}{3}
 \TabPositions{0.6cm}
 \begin{enumeratea}
 \item \(y=\frac{1}{2}x - 6\)
 \item \(y=\frac{1}{2}x -4\)
 \item \(y=\frac{1}{2}x -1\)
 \item \(y=\frac{1}{2}x\)
 \item \(y=\frac{1}{2}x + 2\)
 \item \(y=\frac{1}{2}x + 5\)
 \end{enumeratea}
\end{multicols}

Confrontando cosa cambia e cosa resta uguale nei due gruppi di equazioni 
e di rette possiamo concludere che nell'equazione~\(y=mx+q\):

\begin{itemize}
 \item il coefficiente~\(q\) indica il punto in cui la retta interseca 
  l'asse~\(y\) e viene anche detto \emph{intercetta} o \emph{termine noto};
 \item il coefficiente~\(m\) è legato alla \emph{pendenza} della retta
  e viene anche detto \emph{coefficiente angolare};
\end{itemize}

\subsection{Il coefficiente angolare}

Sul coefficiente angolare possiamo fare alcune osservazioni:

\begin{enumerate}
 \item se è positivo la retta è crescente;
 \item se è negativo la retta è decrescente;
 \item se non è né positivo né negativo la retta non è né crescente né
 decrescente, è costante;
 \item più si avvicina a zero più la retta si avvicina all'orizzontale;
 \item più si allontana da zero, sia in positivo (crescendo) sia in
  negativo (decrescendo), più la retta si avvicina alla verticale;
 \item non esiste alcun coefficiente angolare che produca 
  una retta verticale.
\end{enumerate}

\begin{inaccessibleblock}[Figura: TODO]
 \begin{figure}[h]
 \centering\input{\folder lbr/fig007_coeffang.pgf}
 \caption{Coefficienti angolari.}\label{fig:coeffang}
\end{figure}
\end{inaccessibleblock}

Se consideriamo una retta, ad es.~\(y=\frac{3}{2}x -2\) e alcuni suoi punti
ad es.~\(A(-3;~-4)\), \(B(-1;~-1)\), \(C(1;~2)\), \(D(-3;~5)\), possiamo osservare che 
il rapporto tra gli incrementi delle ordinate e delle ascisse, 
cioè l'aumento dell'ordinata diviso l'aumento dell'ascissa, è sempre lo 
stesso vedi figura: \ref{fig:rappincr}.

\begin{inaccessibleblock}[Figura: TODO]
 \begin{figure}[h]
 \centering
 \begin{minipage}[]{.50\textwidth}
\(\dfrac{y_B - y_A}{x_B - x_A} = \frac{3}{2} = m\)

\(\dfrac{y_C - y_A}{x_C - x_A} = \frac{6}{4} = \frac{3}{2} = m\)

\(\dfrac{y_D - y_A}{x_D - x_A} = \frac{9}{6} = \frac{3}{2} = m\)
  \caption{Tre rapporti incrementali sulla stessa retta.}\label{calc:rappincr}
 \end{minipage}
 \begin{minipage}[]{.40\textwidth}
   \centering\input{\folder lbr/fig008_rappincr.pgf}
  \caption{\(\frac{\Delta y}{\Delta x}\).}\label{fig:rappincr}
 \end{minipage}
\end{figure}
\end{inaccessibleblock}

In generale, dati due punti qualunque di una retta:

\[m = \frac{\Delta y}{\Delta x}\]

\subsection{Disegno rapido}

L'ultima osservazione ci permette di usare un metodo rapido per disegnare 
le rette, un metodo applicabile quando il coefficiente angolare è una 
frazione e l'intercetta un numero intero (la maggior parte degli esercizi 
propone rette di questo tipo). Questo metodo ci mette in grado di disegnare 
una retta in 10 secondi circa. Per ottenere questi tempi deve permetterci di 
disegnare la retta senza farci fare calcoli, perché il nostro cervello non è 
adatto a fare calcoli.

\begin{procedura}
 Disegna la retta che ha per equazione:~\(y=mx+q\):
 \begin{enumeratea}
  \item individua:~\(q\),~\(\Delta x\)~e~\(\Delta y\)
  \item disegna sull'asse~\(y\) il punto di ordinata~\(q\)
  \item a partire da questo punto conta~\(\Delta x\) quadretti verso destra
   e~\(\Delta y\) quadretti verso l'alto segna questo punto;
  \item ripeti l'operazione~c) per trovare altri punti sia a destra sia
   a sinistra dell'asse~\(y\).
  \item disegna la retta che passa per quei punti, 
   vedi figura~\ref{fig:metodorapido}.
 \end{enumeratea}
\end{procedura}

\begin{inaccessibleblock}[Figura: TODO]
 \begin{figure}[h]
 \centering
 \begin{minipage}[]{.30\textwidth}
  \begin{center}
   \(r:~y = - \frac{2}{3}x + 4\)
   
   \(q = 4\)
   
   \(\Delta x = 3\)
   
   \(\Delta y = -2\)
   
   (andare verso l'alto di~\(-2\) significa...)
  \end{center}
  \caption{Elementi da individuare.}\label{fig:elementi}
 \end{minipage}
 \begin{minipage}[]{.60\textwidth}
   \centering\input{\folder lbr/fig009_metodorapido.pgf}
  \caption{Metodo rapido.}\label{fig:metodorapido}
 \end{minipage}
\end{figure}
\end{inaccessibleblock}

\newpage %-----------------------------------------------

\section{Rette parallele e perpendicolari}
\label{sec:retta_paralleleleperpendicolari}

Se abbiamo capito il significato di coefficiente angolare, non è difficile, 
guardando l'equazione di due rette dire se sono parallele. Nel seguente 
elenco evidenzia con colori diversi le rette parallele:

\begin{multicols}{4}
 \TabPositions{0.6cm}
 \begin{enumeratea}
 \item \(y=-\frac{1}{2}x + 7\)
 \item \(y=-\frac{2}{3}x + \frac{5}{3}\)
 \item \(y={3}x + 2\)
 \item \(y=\frac{4}{6}x + 3\)
 \item \(y=\frac{1}{3}x + \frac{5}{3}\)
 \item \(y=-\frac{1}{2}x + 3\)
 \item \(y=-\frac{2}{3}x + 7\)
 \item \(y=\frac{6}{9}x + 2\)
 \item \(-3x + 9y = 0\)
 \item \(2x - 4y + 2 = 0\)
 \item \(10x + 15y + 2\)
 \item \(3x -y + 7 = 0\)
 \end{enumeratea}
\end{multicols}

\begin{definizione}
Due rette sono parallele se e solo se hanno lo stesso coefficiente angolare.
\end{definizione}

Per le rette perpendicolari il problema è più complicato. Partiamo da 
disegnare la retta~\(r\) di equazione~\(y = \frac{4}{5} x\) poi ci procuriamo 
un oggetto dotato di un angolo retto e disegniamo la retta~\(s\) perpendicolare 
a~\(r\) nel punto~\((0;~0)\). Dobbiamo disegnare con la massima precisione. 

Possiamo osservare innanzitutto che se la retta precedente era crescente, la 
perpendicolare sarà decrescente e viceversa. In questo caso il coefficiente 
angolare di~\(s\) sarà negativo. Se abbiamo fatto un buon lavoro con il disegno 
dovremmo trovare che, partendo dal punto in cui~\(r\) interseca l'asse~\(y\), il 
prossimo punto in cui la perpendicolare passa per l'incrocio dei quadretti 
è~\((4;~-5)\). Il coefficiente angolare di~\(s\) è quindi:~\(m_s = - \frac{1}{m_r}\).

\begin{definizione}
Due rette sono perpendicolari se e solo se il coefficiente angolare di 
una è l'\emph{antireciproco} del coefficiente angolare dell'altra.
\end{definizione}

\begin{exrig}
 \begin{esempio}
  Dopo aver riportato su un piano cartesiano i dati dell'esercizio,
  data la retta~\(r:~y = \frac{4}{5} x\), calcola l'equazione della retta~\(s\)  
  parallela a~\(r\) passante per~\(P(-4;~3)\).
 \end{esempio}
 \begin{esempio}
  Dopo aver riportato su un piano cartesiano i dati dell'esercizio,
  data la retta~\(r:~y = \frac{4}{5} x\), calcola l'equazione della retta~\(n\)  
  perpendicolare a~\(r\) passante per~\(P(-4;~3)\).
 \end{esempio}
\end{exrig}

\section{Retta per due punti}
\label{sec:retta_rettaperduepunti}

Più sopra abbiamo ricordato che per due punti passa una sola retta. 
In quale modo possiamo trovare l'equazione della retta che passa per due 
punti assegnati?

\begin{procedura}
 Calcola l'equazione della retta passante per i punti~A~e~B:
 \begin{enumeratea}
  \item Conoscendo i due punti non è difficile calcolare il coefficiente 
   angolare:~\(m = \frac{y_B - y_A}{x_B - x_A}\)
  \item poi resta da calcolare l'intercetta e per questo possiamo applicare 
   la condizione di passaggio per un 
   punto:~\(y_A = m x_A + q \Leftarrow q = y_A - m x_A\).
 \end{enumeratea}
\end{procedura}


\begin{exrig}
 \begin{esempio}

  \begin{inaccessibleblock}[Figura: TODO]
 \begin{figure}[h]
    \centering\input{\folder lbr/fig010_retta03.pgf}
    \caption{Equazione di una retta.}\label{fig:equazione}
  \end{figure}
\end{inaccessibleblock}

Calcola l'equazione della retta passante per~\(A(-3;~4)\) e~\(B(6;~-2)\) 
(figura:~\ref{fig:equazione}).

Per prima cosa disegna i punti e la retta. È facile prevedere che il 
coefficiente angolare dovrà essere negativo e che l'intercetta dovrà valere
all'incirca due. 

Calcoliamo il coefficiente angolare:

\[m = \frac{\Delta y}{\Delta x} = \frac{y_B - y_A}{x_B - x_A} =
      \frac{-2 - 4}{6 -(-3)} = \frac{-2 - 4}{6 + 3} = \frac{-6}{9} = 
      - \frac{2}{3}
\]

Per trovare~\(q\) e imponiamo che la retta di cui conosciamo~\(m\) passi per~\(A\) 
(si ottiene lo stesso risultato imponendo il passaggio per~\(B\)):

\[y_A = - \frac{2}{3} x_A + q \Leftarrow 
    q = y_A +\frac{2}{3} x_A  
    q = 4 +\frac{2}{3} (-3) 
    q = 4 - 2 = 2 
\]

L'equazione della retta è quindi:

\[y = - \frac{2}{3} x + 2\]

(Come sospettavamo).
 \end{esempio}

% Esiste anche una formula molto comoda che fornisce direttamente l'equazione 
% di una retta passante per due punti senza dover calcolare prima~\(m\) e poi~\(q\). 
% La formula è:
% 
% \[\frac{y - y_A}{y_B - y_A} = \frac{x - x_A}{x_B - x_A}\]

 \begin{esempio}
  Calcola, usando la formuletta, l'equazione della retta passante per gli 
  stessi due punti.
 \end{esempio}
\end{exrig}

\section{Fasci di rette}
\label{sec:retta_fasci}

L'equazione parametrica:~\(y - y_P = m (x - x_P)\) al variare del parametro~\(m\) 
rappresenta senz'altro una retta perché è un'equazione di primo grado nelle 
due incognite~\(x\) e~\(y\). Senz'altro questa retta passa per il punto~\(P\), 
infatti se al posto di~\(x\) e di~\(y\) sostituisco 
rispettivamente:~\(x_P\) e~\(y_P\) l'uguaglianza è verificata:

\[y_P - y_P = m (x_P - x_P) \Leftarrow 0 = m \cdot 0\]

Dunque~\(y - y_P = m (x - x_P)\) è l'equazione di una generica retta passante 
per~\(P\). Al variare di~\(m\) ottengo quasi tutte le rette passanti per~\(P\)\dots 
Perché \emph{quasi} tutte?

\begin{exrig}
 \begin{esempio}
  Dopo aver riportato su un piano cartesiano i dati dell'esercizio,
  scrivi l'equazione del fascio di rete passanti per il punto~\(P(3;~2)\).
  Tra tutte queste calcola l'equazione della retta parallela alla retta 
  passante per i punti~\(A(-4;~1)\) e~\(B(3;~-1)\).
 \end{esempio}
 \begin{esempio}
  Dopo aver riportato su un piano cartesiano i dati dell'esercizio,
  scrivi l'equazione del fascio di rete passanti per il punto~\(P(-23;~1)\).
  Tra tutte queste calcola l'equazione della retta perpendicolare alla retta 
  passante per i punti~\(A(3;~4)\) e~\(B(5;~-2)\).
 \end{esempio}
\end{exrig}

\subsection{Formula della retta per due punti}

A questo punto possiamo combinare due argomenti trattati per risolvere in un
solo passo un problema già affrontato e risolto in due passaggi. 
Possiamo combinare la formula del fascio di rette per un 
punto~\(y - y_P = m (x - x_P)\)
e la formula per calcolare il 
coefficiente angolare~\(m = \frac{y_B - y_A}{x_B - x_A}\) 
Sostituendo~\(m\) nella prima otteniamo:

\[y - y_A = \frac{y_B - y_A}{x_B - x_A} (x - x_A)\]

La formula per del fascio di rette passsanti per un punto permette di 
scriverne una analoga che dà immediatamente l'equazione di una retta passante 
per due punti.

\section{Distanza punto retta}
\label{sec:retta_distanzapuntoretta}

Ricordiamo che la distanza tra un punto e una retta è la lunghezza del 
segmento di perpendicolare compreso tra il punto e la retta.


\begin{procedura}
 Per trovare la distanza del punto~\(P\) dalla retta~\(r\), \emph{basta}:
 \begin{enumeratea}
  \item calcolare l'equazione della retta~\(s\) perpendicolare a~\(r\) passante 
   per~\(P\)
  \item trovare l'intersezione~\(I\) tra le due rette~\(r\) e~\(s\)
  \item calcolare la distanza tra i punti~\(P\) e~\(I\).
 \end{enumeratea}
\end{procedura}

Fortunatamente qualche matematico è riuscito a sintetizzare tutto questo 
procedimento in un'unica formula. 
Dato un punto~\(P(x_P; y_P)\) e 
una retta~\(r: ax+by+c=0\),
la distanza~\(d(P, r)\) tra il punto e la retta si ottiene
dalla seguente formula:

\[d(P, r) = \frac{\lvert ax_P +by_P + c\rvert}{\sqrt{a^2 + b^2}}\]

Il numeratore è ottenuto partendo dall'equazione implicita della retta 
sostituendo le variabili con le coordinate del punto~\(P\).

\begin{exrig}
 \begin{esempio}
  Dopo aver riportato su un piano cartesiano i dati dell'esercizio,
  calcola la distanza tra~\(P(-1;~5)\) e~\(r:~y = \frac{1}{3} x -2\). 
 \end{esempio}
 \begin{esempio}
  Dopo aver riportato su un piano cartesiano i dati dell'esercizio,
  calcola la distanza tra~\(P(4;~-3)\) e~\(r:~y = \frac{-1}{5} x +5\),
 \end{esempio}
\end{exrig}

\section{Intersezione di rette}
\label{sec:retta_intersezionedirette}

Ritorniamo dove eravamo partiti: i punti di una rette sono tutti e soli quei 
punti le cui coordinate sono soluzioni dell'equazione. Se due rette hanno un 
punto in comune questo significa che le coordinate di quel punto sono 
soluzione di entrambe le equazioni. Trovare le coordinate del punto che due 
rette hanno in comune significa trovare le soluzioni comuni alle due equazioni.

\begin{exrig}
 \begin{esempio}
  Disegna le due rette~\(r:~y = -\frac{1}{3} x +3\) e~\(s:~y = \frac{4}{3} x -2\) 
  individua graficamente l'intersezione e verifica che 
  le sue coordinate sono soluzioni di entrambe le equazioni.
\begin{inaccessibleblock}[Figura: TODO]
 \begin{figure}[h]
 \centering
 \begin{minipage}[]{.60\textwidth}
   \centering\input{\folder lbr/fig011_intersezione01.pgf}
  \caption{Intersezione di due rette.}\label{fig:intersezione01}
 \end{minipage}
 \begin{minipage}[]{.30\textwidth}
  \begin{center}

   \(y = -\dfrac{1}{3} x +3 \newline
    2 = -\dfrac{1}{3} 3 +3 = -1 + 3\)
   
   \(y = \dfrac{4}{3} x -2 \newline
    2 = \dfrac{4}{3} 3 -2 = 4 -2\)

  \end{center}
  \caption{Verifica dell'intersezione.}\label{fig:elementi}
 \end{minipage}
\end{figure}
\end{inaccessibleblock}
 \end{esempio}
 
\vspace{-12pt}

Dopo aver disegnato le due rette si vede immediatamente che si intersecano 
nel punto~\(I(3;~2)\). Sostituendo~3 alla~\(x\) e~2 
alla~\(y\) nelle due equazioni otteniamo:

\(y = -\frac{1}{3} x +3 \sRarrow
2 = -\frac{1}{3} 3 +3 = -1 + 3 = 2\)
\qquad
\(y = \frac{4}{3} x -2 \sRarrow
2 = \frac{4}{3} 3 -2 = 4 -2 = 2\)
   
Ovviamente il metodo appena utilizzato non è generale: come facciamo a trovare 
le coordinate esatte se l'intersezione non cade esattamente sul vertice di  
un quadretto? Rovesciamo il problema: per individuare il punto 
cerchiamo i due numeri che risolvono entrambe le equazioni, quei due numeri
sono le coordinate dell'intersezione delle rette.

In matematica per indicare che due frasi devono essere contemporaneamente 
vere si usa il simbolo di una grande parentesi graffa aperta che le racchiuda 
e l'insieme di più equazioni che devono essere vere contemporaneamente viene
chiamato sistema. Risolvere un sistema significa trovare quei numeri che 
messi al posto delle incognite rendono vere tutte le uguaglianza.

Alla soluzione dei sistemi è dedicato tutto il prossimo capitolo, ma possiamo 
intanto anticipare uno dei trucchi che useremo: se nella prima equazione c'è 
scritto che~y è uguale a un'espressione, nella seconda equazione, al posto 
di~y possiamo scrivere quella espressione. Vediamo questo procedimento con un 
esempio.

 \begin{esempio}
  Disegna le due rette~\(r:~y = \dfrac{3}{2} x +2\) e~\(s:~y = \dfrac{2}{3} x -1\) 
  calcola le coordinate dell'intersezione e verifica di aver
  ottenuto una soluzione credibile.
\begin{inaccessibleblock}[Figura: TODO]
 \begin{figure}[h]
 \centering
 \begin{minipage}[]{.60\textwidth}
   \(\bigg \{
     \begin{array}{l}
      y = \dfrac{3}{2} x +2 \\
      y = \dfrac{2}{3} x -1
     \end{array}
   \)
  
   \(\dfrac{3}{2} x +2 = \dfrac{2}{3} x -1 \newline
    \dfrac{3}{2} x - \dfrac{2}{3} x = -1 -2 \newline
    \dfrac{5}{6} x = -3 \newline
    x = -\dfrac{18}{5} = -3,6\)
   
   \(y = \dfrac{2}{3} (-\dfrac{18}{5}) -1 \newline
    y = - \dfrac{12}{5} -1 = - \dfrac{17}{5} = -3,4\)
  \caption{Calcolo dell'intersezione.}\label{fig:elementi}
 \end{minipage}
 \begin{minipage}[]{.30\textwidth}
   \centering\input{\folder lbr/fig012_intersezione02.pgf}
  \caption{Intersezione di due rette.}\label{fig:intersezione02}
 \end{minipage}
\end{figure}
\end{inaccessibleblock}
 \end{esempio}
 
\vspace{-12pt}

   Dopo aver disegnato le due rette si vede immediatamente che si intersecano 
   circa nel punto~\(I(3;~2)\). Risolviamo il sistema:
   
   \(\bigg \{
     \begin{array}{l}
      y = \frac{3}{2} x +2 \\
      y = \frac{2}{3} x -1
     \end{array}
   \)
  
   La prima equazione ci dice che~\(y\) è equivalente a~\(\frac{3}{2} x + 3\) 
   quindi, nella seconda equazione al posto di~\(y\) 
   scriviamo:~\(\frac{3}{2} x + 3\) otteniamo così un'equazione che contiene 
   una sola incognita, l'ascissa dell'intersezione:
   
   \(\frac{3}{2} x +2 = \frac{2}{3} x -1 \newline
    \frac{3}{2} x - \frac{2}{3} x = -1 -2 \newline
    \frac{5}{6} x = -3 \newline
    x = -\frac{18}{5} = 3,6\)
   
   E sostituendo questo valore in una delle due equazioni troviamo anche il 
   valore dell'ordinata dell'intersezione:
   
   \(y = \frac{2}{3} (-\frac{18}{5}) -1 
      = - \frac{12}{5} -1 = - \frac{17}{5} = 3,4\)
\vspace{6pt}
\end{exrig}

% \section{Segno di una retta}
% \label{sec:08_segno}
