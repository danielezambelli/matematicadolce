% (c) 2015 Daniele Zambelli daniele.zambelli@gmail.com

\input{\folder integrazione_grafici.tex}

% \begin{wrapfloat}{figure}{r}{0pt}
% \includegraphics[scale=0.35]{img/fig000_.png}
% \caption{...}
% \label{fig:...}
% \end{wrapfloat}
% 
% \begin{center} \input{\folder lbr/fig000_.pgf} \end{center}

\chapter{Integrali} 

\section{Un problema di area}
\label{sec:integrali_area}

\footnote{Per scrivere questo capitolo mi sono ispirato 
all'articolo ``Elementi di Analisi Non Standard, l'integrale'' apparso sul 
periodico Matematicamente della Mathesis di Verona. 
Chi volesse approfondire l'argomento può trovare l'articolo 
all'indirizzo: 
\href{http://mathesisverona.it/Numeri/Nume210.pdf}
     {mathesisverona.it/Numeri/Nume210.pdf}}
Uno dei primi problemi affrontati usando il piano cartesiano è stato quello 
di determinare l'area sottesa ad un segmento. In questo capitolo vogliamo 
riprendere quel tipo di problemi e generalizzarlo alla ricerca dell'area 
sottesa ad una funzione qualsiasi.

Quando diciamo area sottesa ad un segmento intendiamo la parte di piano 
delimitata dal segmento, dalle rette parallele all'asse \(y\) passanti per i 
suoi estremi e dall'asse \(x\).

\begin{esempio}
Dati i punti \(A \punto{2,5}{6}\) e \(B \punto{4,3}{7,2}\), calcola l'area 
sottesa al segmento \(AB\).

\begin{minipage}{.29\textwidth}
\begin{inaccessibleblock}[Area sottesa al segmento.]
  \areasottesasegmento
\end{inaccessibleblock}
\end{minipage}
\hfill
\begin{minipage}{.69\textwidth}
 Possiamo riconoscere che la superficie sottesa al segmento ha la forma di un 
trapezio rettangolo con le due basi lunghe \(y_A\) e \(y_B\) e l'altezza 
lunga \(b - a = x_B - x_A\). L'area è quindi:
\begin{align*}
 \mathcal{A} &= \frac{1}{2} \cdot \tonda{y_B + y_A} \cdot \tonda{x_b - x_a}=\\
             &= \frac{1}{2} \cdot \tonda{7,2 + 6} \cdot \tonda{4,3 - 2,5} =\\
             &= \frac{1}{2} \cdot \tonda{13,2} \cdot \tonda{1,8} = 11,88
\end{align*}
\end{minipage}
\end{esempio}

Abbiamo anche calcolato l'area della superficie sottesa ad una sequenza di 
segmenti consecutivi sommando le varie aree sottese. Ma non è necessario che 
i segmenti siano consecutivi, possono anche essere staccati uno dall'altro.

\begin{esempio}
Calcola l'area sottesa ai segmenti paralleli all'asse \(x\), di lunghezza 
\(0,5\) e di ordinata rispettivamente: \quad 
\(5;~4;~6;~3;~7;~5\)

\begin{minipage}{.29\textwidth}
\begin{inaccessibleblock}[Area sottesa al segmento.]
  \areasottesasegmenti
\end{inaccessibleblock}
\end{minipage}
\hfill
\begin{minipage}{.69\textwidth}
Prima di affrontare i calcoli possiamo fare alcune osservazioni:
\begin{enumerate} [nosep]
 \item i vari segmenti non sono in continuità uno con l'altro;
 \item formano delle colonne rettangolari;
 \item le colonne sono appoggiate le une alle altre e non si sovrappongono;
 \item l'area complessiva si può calcolare sommando le aree di tutte le 
colonne;
 \item la larghezza complessiva delle colonne è~3;
 \item la colonna più bassa ha altezza 3;
 \item la colonna più alta ha altezza 7;
 \item di sicuro l'area è maggiore di \(3 \cdot 3\);
 \item di sicuro l'area è minore di \(3 \cdot 7\);
\end{enumerate}
\end{minipage}
\end{esempio}

La soluzione consiste nel calcolare l'area di ogni rettangolo e sommarli 
tutti.
Chiamiamo:
\(\mathcal{A}_0\) l'area sottesa al segmento \(S_0\),\\
\(\mathcal{A}_1\) l'area sottesa al segmento \(S_1\),\\
\dots\\
\(\mathcal{A}_i\) l'area sottesa al segmento \(S_i\),\\
\dots\\
\(\mathcal{A}_n\) l'area sottesa al segmento \(S_n\)
\quad
(dove con \(n\) intendiamo l'ultimo segmento).\\

Possiamo calcolare allora l'area con la seguente formula:
\begin{align*}
  \mathcal{A} &= \mathcal{A}_0 + \mathcal{A}_1 + \dots + 
                 \mathcal{A}_i + \dots + \mathcal{A}_n = \\
              &= y_0 \cdot \Delta_x + y_1 \cdot \Delta_x + 
                 y_2 \cdot \Delta_x + y_3 \cdot \Delta_x + 
                 y_4 \cdot \Delta_x + y_5 \cdot \Delta_x = \\
              &= ~5 \cdot 0,5\, + ~4 \cdot 0,5\, + ~6 \cdot 0,5\, + 
                 ~3 \cdot 0,5 + ~7 \cdot 0,5 + ~5 \cdot 0,5 = \\
              &= \quad\; 2,5 \; + \quad\; 2 \quad\; + \quad\; 3 \quad\, + 
                 \quad\, 1,5 \; + \quad\, 3,5 \; + \quad\, 2,5 \; = 15
\end{align*}

Le scritture usate per le formule scritte sopra non piacciono ai matematici 
perché risultano lunghe da scrivere e nascondono, 
in mezzo a molti simboli che si ripetono, 
la sostanza delle operazioni che vengono fatte . 
È stato così inventato un altro simbolo detto 
\emph{Sommatoria} che indica la somma di un certo numero di espressioni che 
si assomigliano. Il simbolo usato è la \emph{Sigma} (la esse greca) maiuscola:

\[\sum\]

Intorno a questo simbolo vengono aggiunte tutte le informazioni che servono 
per precisare il calcolo:

\begin{itemize} [nosep]
 \item \emph{sotto}: il nome dell'indice e il suo primo valore, nel nostro 
caso:~\(i=0\);
 \item \emph{sopra}: l'ultimo valore dell'indice, nel nostro caso:~5;
 \item \emph{dopo}: cosa si somma, nel nostro caso: la formula per calcolare 
l'area di ogni rettangolo da sommare: \(y_i \cdot \Delta_x\).
\end{itemize}

\[\mathcal{A} = \sum_{i=0}^5 \mathcal{A}_i = 
                \sum_{i=0}^5 y_i \cdot \Delta_x = 15\]

\ifcoding
Con il linguaggio di programmazione Python:
\lstinputlisting[firstline=24]{\folder lbr/areasottesasegmenti.py} %, 
%  lastline=5]
Che può essere tradotto: somma 
\(\tonda{y_i \cdot delta_x}\) con \(y_i\) che va 
dal primo all'ultimo valore contenuto in ordinate.
\fi

\section{L'area sottesa ad una funzione}
\label{sec:integrali_definiti}

Calcolare l'area sottesa ad un segmento è un problema piuttosto banale, 
l'avevamo già risolto diversi anni fa, ma nel mondo reale ci sono poche 
linee rette, la vita è tutta una curva!
Se vogliamo risolvere teoricamente un problema che abbia utilità pratica 
dobbiamo cercare di calcolare l'area sottesa ad una funzione qualunque.

Ed è quello che intende fare lo studio degli \emph{integrali definiti}.
L'integrale definito di una funzione \(f\) è proprio
l'area compresa tra il grafico della funzione, l'asse delle ascisse 
e due rette verticali per gli estremi \(a\) e \(b\) di un intervallo chiuso 
in cui la funzione è definita.

Fissata la funzione f, tale area dipenderà dalla scelta degli estremi a e b 
dell'intervallo, ed è possibile indicarla come: 
\[\mathcal{A}(a,b)\]

La funzione binaria \(\mathcal{A}\), area sottesa ad una funzione in un 
intervallo, ha due tipiche proprietà che potranno essere usate per 
determinarla. Esse sono: 

\begin{enumerate}
 \item
La \emph{proprietà additiva}, dice che se \(a\), \(b\) e \(t\) 
sono tre valori che appartengono all'intervallo in cui è definita \(f\) e 
\(a < t < b\)
allora \(\mathcal{A}(a,~b)\) = 
\(\mathcal{A}(a,~t)\)+\(\mathcal{A}(t,~b)\)).

\begin{minipage}{.49\textwidth}
\begin{inaccessibleblock}[Area sottesa ad una curva.] 
  \areasottesacurva
\end{inaccessibleblock}
\end{minipage}
\hfill
\begin{minipage}{.49\textwidth}
\begin{inaccessibleblock}[Proprietà additiva per 
un'area sottesa ad una curva.] 
  \propradditiva
\end{inaccessibleblock}
\end{minipage}

Questa proprietà afferma che se una superficie 
viene divisa in due parti da una linea, 
allora l'area totale è la somma delle aree delle due parti.
Nel nostro caso la linea è la retta di equazione: \(x = t\).

 \item 
La \emph{proprietà rettangolare} rispetto alla funzione \(f\), 
dice che se \(m\) è il minimo e \(M\) massimo della funzione \(f\) 
nell'intervallo \(\intervcc{a}{b}\)
allora \\
\(m\cdot(b-a) = \mathcal{A}_{mi}(a,~b) \leq \mathcal{A}(a,~b) \leq
                       \mathcal{A}_{ma}(a,~b) = M\cdot(b-a)\),

\begin{minipage}{.49\textwidth}
\begin{inaccessibleblock}[Rettangolo minore dell'area sottesa.] 
  \areaminore
\end{inaccessibleblock}
\end{minipage}
\hfill
\begin{minipage}{.49\textwidth}
\begin{inaccessibleblock}[Rettangolo maggiore dell'area sottesa.] 
  \areamaggiore
\end{inaccessibleblock}
\end{minipage}

Questa proprietà afferma che l'area sottesa ad una funzione è maggiore 
dell'area di un rettangolo di ugual base che ha per altezza il minimo della 
funzione \(f\) nell'intervallo \(\intervcc{a}{b}\) 
e minore di un rettangolo con la stessa base che ha per altezza il massimo 
della funzione \(f\) nell'intervallo \(\intervcc{a}{b}\). 
\end{enumerate}

\section{Definizione}
\label{sec:integrali_definizione}

\begin{minipage}{.49\textwidth} 
Consideriamo una funzione \(f\) definita in tutto un intervallo \(I\) a cui 
appartengono i punti \(a\) e \(b\), e una partizione dell'intervallo chiuso 
\(\intervcc{a}{b}\) in sotto intervalli di uguale ampiezza \(\Delta\) 
separati dai punti: 
\(a = t_0 < t_1 < \dots < t_h \le b\) \\
dove \(t_i = t_0 +i \Delta\) \quad e \quad \(t_{i+1} - t_i= \Delta\) \\
e dove \(h\) è il massimo numero naturale tale che: \\
\(t_0 + h \Delta \le b\), e dunque \(b-t_h \le \Delta\). 
Il segmento \(ab\) è dunque uguale alla somma di \(h\) segmenti di lunghezza 
\(\Delta\) più il segmento che va da \(t_h\) a \(b\):
\(ab = h \cdot \delta + (b - t_h)\)
\end{minipage}
\hfill
\begin{minipage}{.49\textwidth}
\begin{inaccessibleblock}[Somma di Riemann.] 
  \sommariemann
\end{inaccessibleblock}
\end{minipage}

Si può considerare la sommatoria:
\[\left(\sum_{i=0}^{h-1} f(t_i)\cdot\Delta \right)+f(t_h)\cdot(b-t_h)\] 
che è l'area dell'unione dei rettangoli che 
hanno per altezza \(f(t_i)\) 
e per base i segmenti di lunghezza \(\Delta\)) e un segmento di lunghezza \(b 
- t_{h}\). 

Chiamiamo questa area \emph{somma di Riemann finita}.

Abbiamo già dimostrato che una funzione \(f\) continua in 
\(\intervcc{a}{b}\) in un intervallo chiuso ha massimo e minimo. 
Chiamiamo \(M\) il massimo e \(m\) il minimo di \(f\) nell'intervallo 
\(\intervcc{a}{b}\).

Allora anche per la somma di Riemann finita vale la proprietà rettangolare: 
\[m\cdot(b - a) \le \sum_a^b f \cdot\Delta \le M\cdot(b - a)\] 
Quindi una somma di Riemann finita è senz'altro un numero finito.

Questa sommatoria dipende:
% \begin{multicols}{3} 
\begin{itemize} [nosep]
 \item dalla funzione \(f\),
 \item dagli estremi \(a\) e \(b\),
 \item dall'intervallo \(\Delta\).
\end{itemize}
% \end{multicols}

Poiché \(h\) e la successione dei numeri \(t_i\) determinati 
dalle quantità indicate, sicché la denoteremo come 
\[\sum_a^b f \cdot \Delta\] 
Se fissiamo la funzione \(f\), e gli estremi \(a\) e \(b\) allora la somma di 
Riemann è una funzione reale unaria che dipende solo dall'intervallo  
\(\Delta\). 

% ESEMPI DI SOMME DI RIEMANN AL VARIARE DI DELTA % TODO

Possiamo osservare che al diminuire della distanza \(\Delta\) tra i punti di 
partizione l'area coperta da questa unione di rettangoli sarà sempre più 
vicina all'area sottesa alla funzione.

Usando i numeri iperreali possiamo dare a \(\Delta\) un valore infinitesimo e 
per distinguerlo lo chiameremo: \(dt\).

Chiamiamo \(H\) il numero infinito di partizioni dell'intervallo 
\(\intervcc{a}{b}\) tale che \(x_H = t_0 + H \cdot dt\) e \(t_H \le b\).

Chiamiamo \emph{somma di Riemann infinita} questa nuova sommatoria:
\[\sum_a^b f \cdot dt\]
Per il principio di transfer, anche per la somma di Riemann infinita vale 
che 
\[m \cdot (b - a) \le  \sum_a^b f \cdot dt  \le M\cdot(b - a)\]
e pertanto anche la somma di Riemann infinita sarà un iperreale finito. 
Possiamo quindi calcolare la sua parte standard. 

A questa parte standard diamo il nome di \emph{integrale definito}
\begin{definizione}
Data una funzione standard \(f\) continua su \(\intervcc{a}{b}\),
chiamiamo \emph{integrale definito} la parte standard della 
somma di Riemann infinita:
\[\pst{\sum_a^b f \cdot dt}\]
\end{definizione}

Per semplificare un po' la notazione useremo per indicare questa quantità un 
nuovo simbolo, una specie di esse stirata che è il simbolo di integrale:
\[\int_a^b f \cdot dt = \pst{\sum_a^b f \cdot dt}\]

\section{Somme di Riemann inferiore e superiore}
\label{sec:integrali_somme_riemann}

Mentre le somme di Riemann finite dipendono dal valore di \(\Delta\), i 
matematici hanno dimostrato che le somme di Riemann infinite non dipendono 
dall'infinitesimo scelto come base dei rettangolini.

Ma, anche accettando la precedente affermazione, il lettore attento e pignolo 
può avere il dubbio che definendo somme di 
Riemann in modo diverso si ottengano integrali definiti diversi. 
Noi abbiamo 
calcolato l'area dei rettangolini prendendo come altezza il valore di \(f\) 
nell'estremo sinistro dell'intervallo, ma cosa succede se consideriamo 
l'estremo destro o se consideriamo un punto a caso dell'intervallo?

Vediamo qualche esempio:
% 
% ESEMPI DI SOMME DI RIEMANN FINITE CON SCELTE DIVERSE DI F(Xi) % TODO

Se in ogni sottointervallo chiamiamo \({}_{-}f(t_i)\) il minimo valore 
assunto dalla funzione e \({}^{-}f(t_i)\) il suo massimo valore, allora 
possiamo definire le somme di Riemann finite superiore e inferiore. \\
La somma di Riemann finita inferiore è: 
\[\sum_{i=0}^h{}_{-}f(t_i)\cdot\Delta = \sum_a^b{}_{-} f \cdot \Delta\] 
e quella superiore è: 
\[\sum_{i=0}^h {}^{-}f(t_i)\cdot\Delta = \sum_a^b {}^{-}f \cdot \Delta\]
Evidentemente 
\[\sum_a^b{}_{-}f \cdot \Delta \le \sum_a^b f \cdot \Delta \le 
\sum_a^b {}^{-}f \cdot \Delta\] 
poiché ogni rettangolino della prima somma ha un'altezza non maggiore 
dell'altezza del corrispondente rettangolino della seconda somma e:

\begin{itemize} [nosep]
 \item per la proprietà rettangolare, l'area sottesa alla funzione in ogni 
sottointervallo, sarà compresa tra il rettangolino inferiore e quello 
superiore.
 \item per la proprietà additiva, l'area sottesa alla funzione sarà maggiore 
della somma di tutti i rettangolini inferiori e minore della somma di tutti i 
rettangolini superiori.
\end{itemize}

\begin{minipage}{.49\textwidth}
\begin{inaccessibleblock}[Somma dei rettangolini contenuti nell'area sottesa.] 
  \riemanninferiore
\end{inaccessibleblock}
\end{minipage}
\hfill
\begin{minipage}{.49\textwidth}
\begin{inaccessibleblock}[Somma dei rettangolini che contengono l'area sottesa.] 
  \riemannsuperiore
\end{inaccessibleblock}
\end{minipage}

  Passando alle loro estensioni naturali non standard, cioè riducendo la base 
di ogni rettangolino a un infinitesimo risulta ancora 
\[\sum_a^b {}_{-}f \cdot dt \le \sum_a^b f \cdot dt \le 
  \sum_a^b {}^{-}f \cdot dt\] 
e si mantengono le proprietà rettangolare e additiva. 
Ma se \(dt\) è un infinitesimo e la funzione è continua, allora anche la 
differenza tra \({}_{-}f(t_0)\) e \({}^{-}f(t_0)\) è un infinitesimo (per la 
definizione di funzione continua) e il prodotto tra questa differenza e 
l'intervallo \(dt\) è un infinitesimo di ordine superiore 
all'area di un rettangolino perciò anche la somma, 
pur infinita, di questi prodotti è un infinitesimo. 
Quindi la differenza tra la somma di Riemann infinita 
superiore e inferiore è un infinitesimo mentre il valore di una di queste 
somme è un valore finito perciò le due somme sono indistinguibili e la loro 
parte standard è la stessa.
\[\sum_a^b {}_{-}f \cdot dt \sim \sum_a^b {}^{-}f \cdot dt \sRarrow 
  \int_a^b {}_{-}f \cdot dt = \int_a^b {}^{-}f \cdot dt\]
Possiamo perciò chiamare integrale la parte standard di una una qualsiasi 
somma di Riemann infinita:
\[\int_b^a f \cdot dt = \pst{\sum_a^b {}_{-}f \cdot dt} =
                        \pst{\sum_a^b {}^{-}f \cdot dt}\]

\section{Proprietà degli integrali}
\label{sec:integrali_proprieta}

\subsection{Proprietà rettangolare}
\label{subsec:integrali_proprieta_rettangolare}

Partendo dalle disuguaglianze relative alla somma di Riemann infinita:
\[m \cdot (b - a) \le  \sum_a^b f \cdot dt  \le M\cdot(b - a)\]
e passando alle parti standard, che 
preservano le disuguaglianze, poiché il primo e l'ultimo termine sono parti 
standard di se stessi in quanto reali, si ha: 
\[m \cdot(b - a) \le  \int_a^b f \cdot dt  \le M\cdot(b - a)\] 
e quindi anche per l'integrale vale la proprietà rettangolare.

\subsection{Altre proprietà }
\label{subsec:integrali_altre_proprieta}

Dalla definizione di integrale seguono le seguenti proprietà:
\begin{enumerate}
 \item se l'intervallo è nullo, sarà nullo anche l'integrale:
\[\int_a^a f \cdot dt=0\] 
 \item l'integrale di una costante \(k\) è uguale all'area del rettangolo:
\[\int_a^b k \cdot dt = k \cdot (b-a)\]
 \item l'integrale di una costante \(k\) per una funzione \(f\) è uguale alla 
costante per l'integrale della funzione:
\[\int_a^b k \cdot f \cdot dt = k\cdot\int_a^b f \cdot dt\]
 \item l'integrale della somma di due funzioni è uguale alla somma degli 
integrali:
\[\int_a^b (f+g)\cdot dt = \int_a^b f \cdot dt + \int_a^b g\cdot dt\]
 \item se, in un certo intervallo, la funzione \(f\) è sempre inferiore alla 
funzione \(g\) allora, in quell'intervallo, anche l'integrale di \(f\) è 
minore dell'integrale di \(g\):
\[f \le g \quad \forall t \in \intervcc{a}{b} \sRarrow 
\int_a^b f \cdot dt \le \int_a^b g\cdot dt\]
\end{enumerate}

\subsection{Definizione di opposto}
\label{subsec:integrali_opposto}

Definiamo poi che, se scambiamo gli estremi di integrazione, otteniamo 
l'opposto dell'integrale definito:
\[\int_b^a f \cdot dt = - \int_a^b f \cdot dt\]

\subsection{Proprietà additiva}
\label{subsec:integrali_proprieta_additiva}

Poiché la scelta del sottointervallo infinitesimo non cambia il valore 
dell'integrale definito, 
nell'integrale vale anche la proprietà additiva, infatti: 
\[\int_a^b f \cdot dt + \int_b^c f \cdot dt = \int_a^c f \cdot 
dt\] 
Passando poi alle sommatorie infinite:
\[\sum_a^b f \cdot dt,\quad \sum_b^c f \cdot dt \quad\text{ e } \quad 
  \sum_a^c f \cdot dt\] 
e prendendo 
\(dt = (b-a)/H\)
con \(H\) ipernaturale infinito, 
i punti di ripartizione degli intervalli \(\intervcc{a}{b}\) e 
\(\intervcc{b}{c}\) coincidono con quelli dell'intervallo \(\intervcc{a}{c}\)
sicché la terza sommatoria è evidentemente la somma delle altre due.
Passando alle loro parti standard, operazione che preserva l'addizione, si 
ottiene il risultato voluto.

\section{Funzione integrale}
\label{sec:integrali_funzione_integrale}

Il valore di un integrale definito può essere calcolato con una certa 
approssimazione attraverso le somme di Riemann finite, ma questo, a volte, 
risulta piuttosto scomodo. I matematici hanno trovato un modo per poter 
calcolare il valore di un integrale lavorando più con i simboli che con i 
numeri. Il trucco consiste nel generalizzare il problema, cioè cercare una 
funzione \(S(x)\) che abbia come valore l'area della superficie sottesa ad 
una funzione \(f\) fino ad un punto \(x\) variabile. La soluzione di questo 
problema generale permette anche di risolvere altri problemi che non 
troverebbero soluzione nei casi particolari.

Il problema consiste nel calcolare l'area sottesa ad una funzione continua in 
funzione del solo secondo estremo:
\[\mathcal{S}(x) = \int_{\dots}^x f~dx \] %= \int f~dx\] 

Con \(\int_{}^x f~dx\) intendiamo l'area sottesa fino a \(x\).

Noi sappiamo che per la proprietà additiva degli integrali l'area sottesa 
fino a \(x\) è uguale alla somma dell'area sottesa fino ad \(a\) più l'area 
sottesa alla funzione tra \(a\) e \(x\):
\[\int_{}^x f~dx = \int_{}^a f~dx + \int_{a}^x f~dx \sRarrow
  \mathcal{S}(\dots ;~x) = \mathcal{S}(\dots ;~a) + \mathcal{S}(a;~x)\]

Apparentemente questo passaggio non ha migliorato la situazione, perché 
abbiamo trasformato la quantità incognita ``integrale fino a \(x\)'' nella 
somma di una quantità definita: ``integrale da \(a\) a \(x\)'' più 
``integrale fino ad \(a\)'' che è ancora una quantità incognita.

D'ora in poi, la \emph{superficie fino a \(x\)} verrà indicata in modo un 
po' 
più semplice:
\[\mathcal{S}(x) = \int_{\dots}^x f~dx = \int f~dx\]

\begin{minipage}{.49 \textwidth} 
Ora, non sappiamo quanto sia \(\mathcal{S}(\dots ;~a)\) cioè la superficie 
sottesa alla funzione fino ad \(a\), ma di sicuro non dipende ad \(x\), cioè, 
al variare di \(x\), rimane costante.

Possiamo perciò concentrarci sul calcolo di \(\int_{a}^x f~dx\) sapendo che a 
seconda della scelta di \(a\) il risultato ottenuto potrà differire, da 
quello atteso, solo per una costante \(\mathcal{C}_a\)
che dipende dalla scelta di \(a\).

Quindi:
\[\int f~dx = \int_{a}^x f~dx + \mathcal{C}_a\]
\end{minipage}
\hfill
\begin{minipage}{.49 \textwidth}
\begin{inaccessibleblock}[] 
  \begin{center} \intfinox \end{center}
\end{inaccessibleblock}
\end{minipage}


\begin{esempio}
Calcola il valore dell'integrale: \(\int_3^x 2~dx\)

Diamo alcuni valori alla \(x\) e osservando la figura possiamo calcolare 
l'area sottesa:

\begin{minipage}{.59 \textwidth} 
\[S(3;~4) = \int_3^4 2~dx = 2\] 
\[S(3;~5) = \int_3^5 2~dx = 4\] 
\[S(3;~6) = \int_3^6 2~dx = 6\]

In generale avremo: 
\[S(3;~x) = \int_3^x 2~dx = 2x -6\]
\end{minipage}
\hfill
\begin{minipage}{.39 \textwidth}
\begin{inaccessibleblock}[] 
  \begin{center} \funzint{2}{3}{4}{6}{2} \end{center}
\end{inaccessibleblock}
\end{minipage}
\end{esempio}

\begin{esempio}
Calcola il valore dell'integrale: \(\int_1^x 2~dx\)

Diamo alcuni valori alla \(x\) e osservando la figura possiamo calcolare 
l'area sottesa:

\begin{minipage}{.59 \textwidth} 
\[S(1;~4) = \int_1^4 2~dx = 6\] 
\[S(1;~5) = \int_1^5 2~dx = 8\] 
\[S(1;~6) = \int_1^6 2~dx = 10\]

In generale avremo: 
\[S(1;~x) = \int_1^x 2~dx = 2x -2\]
\end{minipage}
\hfill
\begin{minipage}{.39 \textwidth}
\begin{inaccessibleblock}[] 
  \begin{center} \funzint{2}{1}{4}{6}{2} \end{center}
\end{inaccessibleblock}
\end{minipage}
\end{esempio}

Le due soluzioni degli esempi precedenti differiscono, come ci aspettavamo, 
solo per una costante che dipende dall'estremo inferiore dell'integrale. 
Tenendo conto di questa osservazione possiamo scrivere:
\[\int 2~dx = 2x + \mathcal{C}\]

Possiamo generalizzare i precedenti esempi con il seguente:

\begin{esempio}
Studia l'integrale della funzione costante: \(f: x \mapsto k\)
% Calcola il valore dell'integrale: \(\int k~dx\)

\begin{minipage}{.59 \textwidth}
\begin{align*}
\int k~dx &= \int_a^x k~dx + \mathcal{C}_a =
             \int_a^x k~dx = S(a,~x) + \mathcal{C}_a = \\
          &= k \cdot \tonda{x -a} + \mathcal{C}_a = 
             kx - ka + \mathcal{C}_a
\end{align*}
Osservando che \(-ka\) non dipende dalla \(x\) ed è quindi una costante, 
possiamo riunire le due costanti in una unica:
\[-ka + \mathcal{C}_a = \mathcal{C}\]
In generale avremo: 
\[\int k~dx = kx + \mathcal{C}\]
\end{minipage}
\hfill
\begin{minipage}{.39 \textwidth}
\begin{inaccessibleblock}[] 
  \begin{center} \funzintk{3.5}{1.2}{4.7}{k} \end{center}
\end{inaccessibleblock}
\end{minipage}
\end{esempio}

\begin{esempio}
Studia l'integrale della funzione: \(f: x \mapsto x\)

\begin{minipage}{.59 \textwidth}
Osservando il disegno vediamo che la superficie ha la forma di un triangolo 
rettangolo isoscele, quindi se la base è \(x\) anche l'altezza vale \(x\).
scegliamo come estremo inferiore il valore~0.
\[S(0;~4)=\int_0^4 x dx = 8 \quad 
  S(0;~5)=\int_0^5 x dx = 12,5 = \dfrac{1}{2}25\] 
\[S(0;~6)=\int_0^6 x dx = 18 \quad 
  S(0;~7)=\int_0^7 x dx = 24,5 = \dfrac{1}{2}49\]

E in generale:
\[S(0;~x)=\int_0^x x dx = \dfrac{1}{2}x^2\]
\end{minipage}
\hfill
\begin{minipage}{.39 \textwidth}
\begin{inaccessibleblock}[]  
  \begin{center} \funzintx{1}{2}{4}{7}{y=x} \end{center}
\end{inaccessibleblock}
\end{minipage}

\begin{minipage}{.59 \textwidth}
Se invece di prendere come estremo inferiore~0 scegliamo il valore~3, 
otteniamo:
\[S(3;~4)=\int_3^4 x dx = 3,5 = \dfrac{1}{2}7\qquad
  S(3;~5)=\int_3^5 x dx = 8\] 
\[S(3;~6)=\int_3^6 x dx = 13,5 = \dfrac{1}{2}27 \qquad 
  S(3;~7)=\int_3^7 x dx = 20\]

E in generale:
\[S(3;~x)=\int_3^x x dx = \dfrac{1}{2}x^2 -4,5\]
\end{minipage}
\hfill
\begin{minipage}{.39 \textwidth}
\begin{inaccessibleblock}[]  
  \begin{center} \funzintx{1}{3}{4}{7}{y=x} \end{center}
\end{inaccessibleblock}
\end{minipage}

Possiamo osservare che le due soluzioni precedenti \(S(3;~x)\) e \(S(0;~x)\)
differiscono solo per una costante quindi: 
\[\int x~dx = \dfrac{1}{2}x^2 + \mathcal{C}\]
\end{esempio}

% ESEMPI DI FUNZIONI SEMPLICI: COSTANTE, LINEARE, QUADRATA % TODO
% 
% SPIEGARE IL CONCETTO DI FUNZIONE INTEGRALE %TODO

\section{Teorema fondamentale dell'analisi}
\label{sec:integrali_teorema_fondamentale}
Osservando i risultati degli esempi precedenti, potrebbe sorgerci un qualche 
sospetto \dots se ci ricordiamo qualcosa sulle derivate.

Ma procediamo cercando di risolvere il problema precedente per una funzione 
qualsiasi: 
\[S(x) = \int f \cdot dx\]

\subsection{Dimostrazione grafica}

\begin{minipage}{.49 \textwidth}
Consideriamo un rettangolino infinitesimo in cui è divisa l'area sottesa alla 
funzione \(f\). chiamiamo:
\[d S(x)\]
la sua area. 

Questo rettangolino infinitesimo avrà base lunga \(dx\) e 
altezza indistinguibile da \(f(x)\) quindi la sua area vale:
\[f(x) \cdot dx\]
\end{minipage}
\hfill
\begin{minipage}{.49 \textwidth}
\begin{inaccessibleblock}[]  
  \begin{center} \teoremafonda \end{center}
\end{inaccessibleblock}
\end{minipage}

Ne deriva:
\[f(x) \cdot dx = d S(x) \sRarrow f(x) = \frac{d S(x)}{dx} \sim S'(x) =
  D\quadra{\int f~dx}\]
A parole: 
\begin{itemize} [nosep]
 \item la funzione integranda è uguale alla derivata dell'integrale;
 \item l'integrale è l'operatore inverso della derivata.
\end{itemize}

Per trovare l'integrale della funzione \(f\) devo trovare una funzione 
\(F\) primitiva di \(f\) cioè una funzione la cui derivata sia \(f\).

\subsection{Dimostrazione algebrica}

Utilizzando la definizione di derivata possiamo
dimostrare che la derivata della funzione integrale è proprio la 
funzione integranda cioè che  
\[D\quadra{\int f~dx} = D\quadra{F(x)} = F'(x) = f(x)\]
Applichiamo alla funzione \(F\) la regola del calcolo della derivata:
\[F'(x) = \pst{\frac{F(x+dx)-F(x)}{dx}} = 
\pst{\frac{\int_a^{x+dx} f \cdot dx - \int_a^x f \cdot dx}{dx}} =\] 
% che per l'arbitrarietà dell'incremento (l'incremento d'integrazione \(dx\) 
% può essere \(du\)) è uguale a
% \[\pst{\frac{\int_a^{x+dx} f \cdot dx - \int_a^x f \cdot dx}{dx}} =\]
per l'additività degli integrali consideriamo che
\[\pst{\frac{\int_x^{x+dx} f \cdot dx}{dx}} = 
    \pst{\frac{f(x) \cdot dx}{dx}} = \pst{f(x)} = f(x)\]

La prima uguaglianza della riga
precedente è giustificata dal fatto che, per definizione 
\[\int_x^{x+dx} f \cdot dx \approx \sum_x^{x+dx} f \cdot dx = f(x) \cdot dx\]

mentre l'ultima è vera poiché \(f(x)\) un numero reale quindi la sua parte 
standard è uguale al numero stesso. 

% Volendo raggiungere l'espressione che consente di calcolare un integrale 
% definito dai valori di una primitiva negli estremi di integrazione, si può 
% osservare che, ricordando ancora che le primitive di una funzione continua in 
% un intervallo differiscono per una costante, si può osservare che
% \[F(b) = \int_a^b f \cdot dx = G(b) + k\]
% dove G è una qualsiasi primitiva di \(f\) e k dipende da G. Per determinare k, 
% si osservi che
% \[0 = \int_a^a f \cdot dx = G(a) + k\]
% sicché deve essere \(k = - G(a)\), e si ottiene nuovamente la classica 
% relazione tra integrale definito e primitive della funzione integranda 
% \[\int_a^b f \cdot dx = G(b) - G(a)\]
% 
% La determinazione mediante le primitive può esser un ottimo modo per 
% calcolare un integrale. Ma anche in questo caso ci sono difficoltà. 
% Non esiste un metodo semplice per calcolare una primitiva.
% 
% Data una funzione \(f\), per trovare una sua primitiva bisogna ricordare di 
% avere incontrato \(f\) come derivata di una  certa funzione. 
% I metodi d'integrazione aiutano a modificare la situazione nella speranza di 
% arrivare al punto di dover trovare primitive di funzioni che si ricorda di 
% aver incontrato come derivate di altre funzioni. 
% La determinazione delle primitive di una funzione, è spesso una sfida aperta 
% e alcune volte è proprio impossibile da calcolare.

\section{Integrali indefiniti}
\label{sec:integrali_indefiniti}

Il teorema fondamentale dell'analisi dice che la funzione integrale è una 
primitiva della funzione integranda quindi se ho imparato a derivare una 
funzione posso, con il procedimento inverso trovare una funzione integrale 
della funzione integranda:
\[\text{se} \quad D \quadra{F} = f \quad \text{allora} \quad \int f dx = F\]

Ma dalle regole delle derivate sappiamo che tutte le funzioni che 
differiscono per una costante hanno la stessa derivata quindi se 
\(D \quadra{F} = f\) allora anche \(D \quadra{F+5} = f\), e anche 
\(D \quadra{F+4,57} = f\), e, in generale, anche \(D \quadra{F+C} = f\) con 
\(C\) costante.
Quindi se \(F\) è una primitiva della funzione \(f\) e \(C\) è una 
costante, allora tutte le funzioni del tipo: \(F + C\) sono primitive di 
\(f\).

Di seguito vediamo una tabella con le primitive di alcune funzioni.

{%
\newcommand{\mc}[3]{\multicolumn{#1}{#2}{#3}}
\begin{center}
\begin{tabular}{ccc}
primitiva & \qquad f \qquad & derivata\\
\(k x +C\) & \qquad \(k\) & \qquad \(0\)\\
\(\dfrac{1}{2}x^2 +C\) & \qquad \(x\) \qquad & \(1\)\\
\(\dfrac{k}{2}x^2 +C\) & \qquad \(kx\) \qquad & \(k\)\\
\(\dfrac{1}{n+1}x^{n+1} +C\) & \qquad \(x^n\) \qquad & \(nx^{n-1}\)\\
\(e^x +C\) & \qquad \(e^x\) \qquad & \(e^x\)\\
\(\dfrac{a^x}{\ln{a}} +C\) & \qquad \(a^x\) \qquad & \(\tonda{\ln{a}} a^x\)\\
\(ln \abs{x} +C\) & \qquad \(\dfrac{1}{x}\) \qquad & \(-\dfrac{1}{x^2}\)\\
\(-\cos x +C\) & \qquad \(\sen x\) \qquad & \(\cos x\)\\
\(\sen x +C\) & \qquad \(\cos x\) \qquad & \(-\sen x\)\\
\(\tg x +C\) & \qquad \(\dfrac{1}{cos^2 x}\) \qquad & 
    \(-\dfrac{2 \cos x \sen x}{cos^4 x}\)\\
\(\) & \qquad \(\) \qquad & \(\)\\
\end{tabular}
\end{center}
}%

\section{Calcolo dell'integrale definito}
\label{sec:integrali_indefiniti}

Avere un modo per calcolare l'integrale indefinito permette di affrontare il 
problema dell'integrale definito in un modo più semplice.
Supponiamo di avere una funzione \(f\) e di conoscere una sua primitiva 
\(F\), cioè: \(F' = f\).
Dato che \(F(a)\) è il valore dell'area fino ad \(a\) e 
\(F(b)\) è il valore dell'area fino ad \(b\), 
per trovare l'area sottesa alla funzione tra \(a\) e \(b\),
basta togliere dall'area fino ad \(a\) l'area fino a \(b\).
\[\text{se} \quad F(x) = \int f(x)~dx \quad \text{allora} \quad 
\int_a^b f(x)~dx = \quadra{F(x)}_a^b = F(b) - F(a)\]

\begin{esempio}
Calcola l'area sottesa alla funzione \quad
\(f: x \mapsto 2 x^2 -x +3\) \quad 
tra~\(-1\) e~\(+2\).

\begin{minipage}{.59 \textwidth}
Calcoliamo un integrale indefinito della funzione \(f\):
\begin{align*} 
\int \tonda{2 x^2 -x +1}~dx &= 2 \int x^2~dx -\int x~dx + \int 1~dx = \\
                         &= \frac{2}{3}x^3 - \frac{1}{2}x^2 + x + \mathcal{C}
\end{align*}
Adesso che abbiamo l'integrale indefinito della funzione, possiamo calcolare 
l'area cercata con una semplice sottrazione: basta togliere dall'area 
sottesa fino a \(b\) l'area sottesa fino ad \(a\):
\end{minipage}
\hfill
\begin{minipage}{.39 \textwidth}
\begin{inaccessibleblock}[]  
  \begin{center} \intdef \end{center}
\end{inaccessibleblock}
\end{minipage}

\begin{align*}
\mathcal{S}_{ab} &= 
  \quadra{\frac{2}{3}x^3 - \frac{1}{2}x^2 + x}_{-1}^{2} = 
  \tonda{\frac{2}{3}2^3 - \frac{1}{2}2^2 + 2} -
     \tonda{\frac{2}{3}(-1)^3 - \frac{1}{2}(-1)^2 +(-1)} = \\
  &= \tonda{\frac{16}{3} - \frac{4}{2} + 2} - 
     \tonda{-\frac{2}{3} - \frac{1}{2} -1} = 
  \frac{32 -12 +12 +4 +3 +6}{6} = \frac{45}{6} = 7,5
\end{align*} 


 
\end{esempio}

\begin{comment}
\section{Osservazioni}

Caro Luciano e cari amici,
       nonostante la revisione che vi ho inviato la scorsa notte, rimangono 
ancora dei punti trattati ap­prossimativamente. Ad esempio uno potrebbe 
rimanere perplesso nel leggere \(\int_u^{u+du} f(x) \cdot du\), 
doman­dandosi che ci sta a 
fare la x in questa espressione. Infatti, a rigore, non dovrebbe esserci. 
\(f(x)\) indica la quantità che si ottiene calcolando la funzione unaria \(f\) 
nel 
valore x della variabile indipendente, mentre la funzione è indicata dalla 
sola \(f\). Così, se si volesse essere precisi fino in fondo si sarebbe 
dovuto 
scrivere \(\int_u^{u+du} f \cdot du\), e non solo in questa occasione. 
Infatti, l'integrale 
vuole rappresentare l'area limitata da una curva che è il grafico della 
funzione \(f\) e non il valore di questa funzione in un certo punto. 
Purtroppo, 
tradizionalmente s'indica impropriamente la funzione come f(x) poiché non si 
fa tanto riferimento alla funzione in sé ma al modo linguistico con cui viene 
descritta. Ad esempio, si usa scrivere \(f(x) = 5x^2+sin x\) per indicare la 
funzione che a un certo valore fa corrispondere 5 volte il quadrato di quel 
valore più il seno dello stesso. Con una notazione più precisa si dovrebbe 
scrivere \(f: x \mapsto 5x^2+sin x\) in cui correttamente la funzione è 
indicata con \(f\) 
e quanto segue i due punti sta a descrivere di quale funzione ci si sta 
interessando e come a un valore ne corrisponde un altro. La stessa idea è 
colta dalla seguente notazione per una funzione, 
\(f = \graffa{(x, 5x^2+sin x): x \in \R}\). 
Ancora si nota la netta separazione tra la notazione della funzione e la 
descrizione del suo comportamento. Uno potrebbe obiettare che nel descrivere 
delle funzioni a volte s'incontrano altre variabili o lettere che indicano 
parametri, e bisogna indicare quale simbolo indica la variabile indipendente 
della funzione unaria che si vuole considerare. Ad esempio vorremmo mantenere 
la scrittura \(f(x) = 5ax^2+sin xy\) in cui il riferimento alla variabile x è 
essenziale. Ma le notazioni 
\(f: x \mapsto 5ax2+sin xy\) e 
\(f=\graffa{(x, 5ax2+sin xy): x \in \R}\), 
che separano il simbolo \(f\) dalla sua definizione, sono altrettanto 
esplicite, 
e, se proprio si vuole enfatizzare che la funzione è una funzione della 
variabile \(x\), si può usare la notazione 
\(f_x\), sicché la scrittura \(f_x(c)\) 
indica il valore che questa funzione assume in corrispondenza del valore 
\(c\) attribuito alla variabile \(x\).
Tornando a noi, tutte le volte che si vuole indicare una funzione si sarebbe 
dovuto scrivere solo \(f\), anche nei numeri precedenti. Così, se abbiamo 
scritto 
imprecisamente \(f(x)\), ciò è dovuto al peso di una certa tradizione che in 
qualche modo giustifica la notazione usata, ma non evita che questa debba 
essere interpretata correttamente da persone ammaestrate a farlo, e che 
inoltre induca imprecisioni. 

Nel numero 207 avevamo correttamente indicato il differenziale e la derivata 
con le notazioni \(df\) e \(df / dx\). 
Avevamo anche usato le notazioni 
\(G(x) = g(f(x)) e G'(x) = g'(f(x))·f'(x)\)  
per indicare la funzione composta e la sua 
derivata. Ciò può essere accettato perché si sta cercando di descrivere la 
funzione ottenuta dai valori che assume quando le componenti assumono certi 
valori in corrispondenza di valori della variabile indipendente, anche se si 
tratta di un abuso di linguaggio e richiede l'addestramento sufficiente per 
superare questa imprecisione.

Nel numero 210, avevamo correttamente indicato le somme con la notazione 
\((\sum_{i=0}^{h-1} f(x_i)\cdot\Delta) + f(xh)\cdot(b-xh)\), 
poiché qui davvero si considerano i 
valori che la funzione \(f\) assume nei punti indicati. Ma dopo, osservando 
che 
la somma dipendeva solo da \(\Delta\) e dagli estremi dell'intervallo, oltre 
che 
dalla funzione f, si era scritto \(\sum_a^b f(x) \cdot \Delta\), e qui ci si 
è chinati 
all'imprecisione della tradizione e dal voler richiamare che il prodotto era 
tra valori specifici della funzione e \(\Delta\) (anche se i valori specifici 
non sono indicati da f(x)) invece di indicare \(\sum_a^b f\Delta\). 
Questa accettazione 
della consuetudine è proseguita coerentemente anche nella notazione 
\(\int_a^b f(x) \cdot dx = st (\sum_a^b f(x) \cdot dx)\), anche se sarebbe 
stato più esatto scrivere 
\(\int_a^b fdx = st(\sum_a^b fdx)\), e similmente in tutto quello che segue. 
Essendoci conformati alla notazione corrente, si può a buona ragione 
continuare a seguire questa notazione, sapendo quali abusi linguistici 
comporta e come sviluppare correttamente il percorso.
La difficoltà che questa scelta comporta è messa in luce proprio dalla 
difficoltà di leggere l'espressione \(\int_u^{u+du} f(x).du\) , difficoltà 
che ha motivato questo mio intervento. 
E il modo giusto di leggere è come se al 
posto di \(f(x)\) fosse scritto solo \(f\) perché quello che si sta dicendo non 
riguarda assolutamente la funzione calcolata in  un punto x (che non si sa 
neppure qual è) ma solo la funzione f. 
Che fare del nostro lavoro dopo tutte queste considerazioni? Niente, e 
incrociare le dita sperando che i lettori leggano nel modo da noi inteso. Ma 
si potrebbe anche inserire una frase al momento opportuno per chiarire che a 
volte bisognerebbe leggere la scrittura f(x) come ci fosse solo la f, poiché 
non si sta applicando la funzione a uno specifico valore x ma si sta solo 
indicando la funzione, però non vedo dove inserirla (e quindi cosa dire 
esattamente) senza appesantire l'esposizione con parentesi potenzialmente 
fuorvianti. Una terza azione potrebbe prevedere di preparare un ulteriore 
lavoro che discuta degli abusi linguistici in matematica; questa scelta 
potrebbe anche essere combinata con una delle precedenti.
  Ruggero

\end{comment}

